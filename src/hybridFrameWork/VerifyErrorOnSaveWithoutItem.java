package hybridFrameWork;

import java.util.concurrent.TimeUnit;

import org.apache.log4j.xml.DOMConfigurator;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Reporter;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import utils.Constant;
import utils.ExcelUtils;
import utils.Log;
import appModule.HomeModule;
import appModule.LoginModule;
import appModule.NewRepeatInvoiceCreationModule;
import appModule.SalesModule;
import appModule.SearchRepeatModule;
/**
 * @author Aravind Sankar
 * Test to verify if the user is not able to save the repeating invoice without entering item details
 */
public class VerifyErrorOnSaveWithoutItem {

	private static WebDriver driver = null;

	/**
	 * @throws Exception
	 */
	@BeforeMethod
	public void beforeMethod() throws Exception {
		
		DOMConfigurator.configure("log4j.xml");
		
		//ensure that the chromedriver is installed and property is set
		String projectPath= System.getProperty("user.dir");
		System.out.println(projectPath);
		String chromeDriverPath=projectPath+"\\chromedriver\\chromedriver.exe";		
		System.setProperty("webdriver.chrome.driver",chromeDriverPath);
		//start the log
		Log.startTestCase("Check Login");
		//initialise the excel file path
		ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_TestData,"Sheet1");
		//create a new chrome driver
		driver= new ChromeDriver();
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		
		//open the xero login page
		driver.get(Constant.URL);
	}
	@Test
	public void main() throws Exception {
		
		//verify if the user is able to login
		LoginModule.Execute(driver);
		driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);

		waitForLoad(driver,"dashboard");	
		
		//If Login was successful, click on Accounts-->Sales in home page 
		HomeModule.Execute(driver);	
		waitForLoad(driver,"receivable");
		
		
		//Click on the Repeating link in sales page
		String ar=SalesModule.Execute(driver);
		waitForLoad(driver,"searchrepeating");

		// Click on New Repeating invoce button
		SearchRepeatModule.Execute(driver);
		waitForLoad(driver,"repeattransaction");
		
		//fill the details regarding invoice 
		NewRepeatInvoiceCreationModule.fillTransactionDetails(driver);
		
		//click on the save button
		NewRepeatInvoiceCreationModule.clickSave(driver);
		
		
		if(checkErrorMessages(driver,"Invoice to cannot be empty.","Invoice To")){
			Reporter.log("Verification passed");
		}
		if(checkErrorMessages(driver,"Description cannot be empty.","Description")){
			Reporter.log("Verification passed");
		}		
		System.out.println("done!!!  "+ar);

		//RepeatTransactions/Edit
	}
	//Wait for the page to load
	
		public static void waitForLoad(WebDriver driver,String urlContains){

			(new WebDriverWait(driver, 10)).until(new ExpectedCondition<Boolean>() {
				public Boolean apply(WebDriver d) {
					return d.getCurrentUrl().toLowerCase().contains(urlContains);
				}
			});
		}
		
		//Check the error messages
		public static Boolean checkErrorMessages(WebDriver driver,String compareText,String field)throws Exception{

			String errors= NewRepeatInvoiceCreationModule.getErrors(driver);

			if(errors.contains(compareText)){

				Reporter.log(" Error Verification Passed for "+ field);
				System.out.println("passed "+field);
				return true;

			}else{
				Reporter.log(" Error Verification Passed for "+ field);
				return false;
			}

		}
		
		@AfterMethod
		public void afterMethod() {
			//driver.quit();
			System.out.println("Done!!!");
		}

	}
