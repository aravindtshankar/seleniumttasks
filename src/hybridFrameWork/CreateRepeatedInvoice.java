package hybridFrameWork;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.WebDriverWait;

import utils.Constant;
import utils.ExcelUtils;
import utils.Log;
import appModule.HomeModule;
import appModule.LoginModule;
import appModule.NewRepeatInvoiceCreationModule;
import appModule.SalesModule;
import appModule.SearchRepeatModule;

public class CreateRepeatedInvoice {

	private static WebDriver driver = null;

	public CreateRepeatedInvoice() {
		// TODO Auto-generated constructor stub
	}

	public static void main(String[] args)throws Exception {

		String projectPath= System.getProperty("user.dir");
		System.out.println(projectPath);
		String chromeDriverPath=projectPath+"\\chromedriver\\chromedriver.exe";
		System.setProperty("webdriver.chrome.driver",chromeDriverPath);
		Log.startTestCase("Check Login 1");
		ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_TestData,"Sheet1");
		driver= new ChromeDriver();
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		driver.get(Constant.URL);
		
		
		LoginModule.Execute(driver);
		driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);

		waitForLoad(driver,"dashboard");		
		HomeModule.Execute(driver);		
		
		waitForLoad(driver,"receivable");
      	
		String ar=SalesModule.Execute(driver);
		
		//RepeatTransactions
		waitForLoad(driver,"searchrepeating");
		
		SearchRepeatModule.Execute(driver);
		
		waitForLoad(driver,"repeattransaction");
		
		NewRepeatInvoiceCreationModule.fillTransactionDetails(driver);
		
		NewRepeatInvoiceCreationModule.fillItemDetails(driver);
		
		NewRepeatInvoiceCreationModule.clickSave(driver);
		//driver.quit();
		System.out.println("done!!!  "+ar);
		
		//RepeatTransactions/Edit
	
	}
	
	public static void waitForLoad(WebDriver driver,String urlContains){
		
		(new WebDriverWait(driver, 10)).until(new ExpectedCondition<Boolean>() {
            public Boolean apply(WebDriver d) {
            	return d.getCurrentUrl().toLowerCase().contains(urlContains);
            }
        });
	}
	
	

}