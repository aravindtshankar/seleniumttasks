package pageObjects;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
/**
 * @author Aravind Sankar
 * Object Repository for the New Repeat Invoice creation page
 * @returns Objects as web elements
 */
public class NewRepeatInvoicePage {

	private static WebElement element = null;
// Repeat this transaction every-Number Entry  // Xpath  .//*[@id="PeriodUnit"]
	public static WebElement txt_RepeatDuration(WebDriver driver){
		element = driver.findElement(By.xpath(".//*[@id='PeriodUnit']"));
		return element;
	}

	//Repeat This Transaction every -- month/week // Xpath .//*[@id="PeriodUnit"]

	public static WebElement txt_SelectDurationType(WebDriver driver){
		element = driver.findElement(By.xpath(".//*[@id='PeriodUnit']"));
		return element;
	}
	
	public static WebElement txt_InvoiceStartDate(WebDriver driver){ //*[@id="StartDate"]
		element = driver.findElement(By.xpath(".//*[@id='StartDate']"));
		return element;
	}
	
	public static WebElement txt_InvoiceDueDay(WebDriver driver){//*[@id="DueDateDay"]
		element = driver.findElement(By.xpath(".//*[@id='DueDateDay']"));
		return element;
	}
	
	public static WebElement chk_saveAsDraft(WebDriver driver){
		element = driver.findElement(By.xpath(".//*[@id='saveAsDraft']"));
		return element;
	}
	
	public static WebElement chk_saveAsAutoApproved(WebDriver driver){
		element = driver.findElement(By.xpath(".//*[@id='saveAsAutoApproved']"));
		return element;
	}
	
	public static WebElement txt_itemInTable(WebDriver driver){//*[@id="ext-comp-1001"]
		element = driver.findElement(By.xpath(".//*[@id='ext-comp-1001']"));
		return element;
	}
	
	public static WebElement div_itemInTable(WebDriver driver){
		return element = driver.findElement(By.xpath("//*[@id='ext-gen19']/div[1]/table/tbody/tr/td[2]/div"));
	}//*[@id="ext-gen62"]
	
	//*[@id="ext-gen61"]--description
	public static WebElement txt_Description(WebDriver driver){
		element = driver.findElement(By.xpath(".//*[@id='ext-gen61']"));
		return element;
	}
	
	public static WebElement div_Description(WebDriver driver){
		return element = driver.findElement(By.xpath(".//*[@id='ext-gen19']/div[1]/table/tbody/tr/td[3]/div"));
	}
	

	
	//*[@id="ext-gen51"]/div[3]/div/span[1]/button--save button
	public static WebElement btn_Save(WebDriver driver){
		element = driver.findElement(By.xpath(".//*[@id='ext-gen51']/div[3]/div/span[1]/button"));
		return element;
	}
	//*[@id="ext-gen69"]
	public static WebElement txt_Qty(WebDriver driver){
		element = driver.findElement(By.xpath(".//*[@id='ext-gen69']"));
		return element;
	}

	public static WebElement txt_UnitPrice(WebDriver driver){
		element = driver.findElement(By.xpath(".//*[@id='ext-gen67']"));
		return element;
	}

	public static WebElement txt_Account(WebDriver driver){
		element = driver.findElement(By.xpath(".//*[@id='ext-gen70]"));
		return element;
	}

	public static WebElement txt_Amount(WebDriver driver){
		element = driver.findElement(By.xpath(".//*[@id='ext-gen19']/div[1]/table/tbody/tr/td[10]/div"));
		return element;
	}
	
	public static WebElement txt_Errors(WebDriver driver){
		element = driver.findElement(By.xpath(".//*[@id='notify01']/div/ul"));
		return element;
	}
	
	public static WebElement txt_PaidToName(WebDriver driver){
		element = driver.findElement(By.xpath(".//input[starts-with(@id,'PaidToName')]"));
		return element;
	}
	public static WebElement div_PaidToName(WebDriver driver){
		element = driver.findElement(By.xpath(".//*[@id='invoiceForm']/div[1]/div[1]"));
		return element;
	}
}