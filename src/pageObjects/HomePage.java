package pageObjects;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import utils.Log;
/**
 * @author Aravind Sankar
 *  Object Repository for the Home page
 * @returns Objects as web elements
 */
public class HomePage {

	private static WebElement element = null;

	public static WebElement link_Dashboard(WebDriver driver){
		element = driver.findElement(By.xpath(".//*[@id='Accounts']"));
		Log.info("Account link element found");
		return element;
	}

	//*[@id="xero-nav"]/div[2]/div[2]/div/ul/li[2]/ul/li[2]/a

	public static WebElement link_Sales(WebDriver driver){
		element = driver.findElement(By.xpath(".//*[@id='xero-nav']/div[2]/div[2]/div/ul/li[2]/ul/li[2]/a"));
		Log.info("Sales link element found");
		return element;
	}

}