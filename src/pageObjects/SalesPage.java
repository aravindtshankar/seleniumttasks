package pageObjects;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
/**
 * @author Aravind Sankar
 * Object repository for the sales-->Invoice-->Repeating Invoice Page
 * @returns The Navigation objects
 */
public class SalesPage {

	private static WebElement element = null;

	//repeating invoice// //*[@id="ext-gen1018"]/div[4]/div/div/h2/div/a[2]
	public static WebElement link_Repeat(WebDriver driver){
		element = driver.findElement(By.xpath(".//*[@id='ext-gen1018']/div[4]/div/div/h2/div/a[2]"));
		return element;
	}

	//*[@id="xero-nav"]/div[2]/div[2]/div/ul/li[2]/ul/li[2]/a

	public static WebElement val_AR(WebDriver driver){
		element = driver.findElement(By.xpath(".//*[@id='ext-gen1018']/div[4]/div/div/div[3]/a[3]/span[2]"));
		return element;
	}

}