package pageObjects;

import org.openqa.selenium.By;

import org.openqa.selenium.WebDriver;

import org.openqa.selenium.WebElement;

/**
 * @author Aravind Sankar
 * Object Repository for the Login page
 * @returns Objects as web elements
 */
public class LoginPage {

	private static WebElement element = null;

	public static WebElement txtbx_MyAccount(WebDriver driver){
		element = driver.findElement(By.id("email"));
		return element;

	}

	public static WebElement txtbx_Password(WebDriver driver){
		element = driver.findElement(By.id("password"));
		return element;
	}
	public static WebElement btn_LogIn(WebDriver driver){	 
		element = driver.findElement(By.id("submitButton"));
		return element;
	}
}