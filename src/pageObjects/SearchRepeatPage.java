package pageObjects;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
/**
 * @author Aravind Sankar
 * Object repository for the sales-->Invoice-->Repeating Invoice Page
 * @returns The button object New Repeat Invoice
 *
 */
public class SearchRepeatPage {

	private static WebElement element = null;	

	
	public static WebElement link_NewRepeat(WebDriver driver){
		element = driver.findElement(By.xpath(".//*[@id='ext-gen31']/a"));
		return element;
	}

}