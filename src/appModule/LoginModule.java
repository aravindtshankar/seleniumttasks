package appModule;

import org.openqa.selenium.WebDriver;

import pageObjects.LoginPage;
import utils.ExcelUtils;

public class LoginModule {
	
	/**
	 * @author Aravind Sankar
	 * Method to Login into the base url
	 */

	public static void Execute(WebDriver driver)throws Exception{
		 
		String sUserName = ExcelUtils.getCellData(1, 1);

		String sPassword = ExcelUtils.getCellData(1, 2);
		System.out.println("username entered");
		
		LoginPage.txtbx_MyAccount(driver).sendKeys(sUserName);
		
		System.out.println("password entered");
		LoginPage.txtbx_Password(driver).sendKeys(sPassword);
		
		System.out.println("Login Clicked");

		LoginPage.btn_LogIn(driver).submit();

	}

}
