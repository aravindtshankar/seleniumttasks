package appModule;

import org.openqa.selenium.WebDriver;

import pageObjects.HomePage;

/**
 * @author Aravind Sankar
 * Method to navigate from home page to the sales page
 */
public class HomeModule {	

	public static void Execute(WebDriver driver)throws Exception{
		HomePage.link_Dashboard(driver).click();
		HomePage.link_Sales(driver).click();
	}

}
