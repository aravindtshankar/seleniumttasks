package appModule;

import org.openqa.selenium.WebDriver;

import pageObjects.SearchRepeatPage;

/**
 * @author Aravind Sankar
 * driver to click on the New Repeat Invoice button
 */
public class SearchRepeatModule {

	public static void Execute(WebDriver driver)throws Exception{
		
	SearchRepeatPage.link_NewRepeat(driver).click();
		
	}

}
