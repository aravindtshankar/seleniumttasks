package appModule;

import org.openqa.selenium.WebDriver;

import pageObjects.SalesPage;
/**
 * @author Aravind Sankar
 * driver to click on the Repeating invoice link
 */
public class SalesModule {

	public static String Execute(WebDriver driver)throws Exception{
		String ar= SalesPage.val_AR(driver).getText();

		SalesPage.link_Repeat(driver).click();
		return ar;
		
	}

}
