package appModule;

import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;

import pageObjects.NewRepeatInvoicePage;
import utils.ExcelUtils;

public class NewRepeatInvoiceCreationModule {
	
	/**
	 * @author Aravind Sankar
	 * Method to fill transaction details in the new repeat invoice creation page
	 */

	public static void fillTransactionDetails(WebDriver driver)throws Exception{

		String selectDurationType= ExcelUtils.getCellData(1, 4);
		String invoiceStartDate= ExcelUtils.getCellData(1, 5);
		String invoiceDueDay= ExcelUtils.getCellData(1, 6);	
		
		System.out.println("Entering details"+selectDurationType + "  "+invoiceStartDate+"  "+invoiceDueDay );
		
		NewRepeatInvoicePage.txt_SelectDurationType(driver).clear();
		NewRepeatInvoicePage.txt_SelectDurationType(driver).sendKeys("5");
		
		NewRepeatInvoicePage.txt_InvoiceDueDay(driver).clear();		
		NewRepeatInvoicePage.txt_InvoiceDueDay(driver).sendKeys("7");
		
		NewRepeatInvoicePage.txt_InvoiceStartDate(driver).clear();
		NewRepeatInvoicePage.txt_InvoiceStartDate(driver).sendKeys(invoiceStartDate);
		
		NewRepeatInvoicePage.chk_saveAsAutoApproved(driver).click();		
		
	}
	
	/**
	 * @author Aravind Sankar
	 * Method to add an item in the new repeat invoice
	 */
	
	public static void fillItemDetails(WebDriver driver)throws Exception{

		String itemName= "101: Car WashLiquid";		
		String paidTo= "Aravind Sankar";
		System.out.println("Field "+itemName );
		
		NewRepeatInvoicePage.txt_PaidToName(driver).sendKeys(paidTo);
		NewRepeatInvoicePage.div_PaidToName(driver).click();
		NewRepeatInvoicePage.div_PaidToName(driver).click();
		NewRepeatInvoicePage.div_PaidToName(driver).click();
		
		NewRepeatInvoicePage.div_itemInTable(driver).click();
		NewRepeatInvoicePage.txt_itemInTable(driver).sendKeys(itemName);	
		NewRepeatInvoicePage.txt_itemInTable(driver).sendKeys(Keys.RETURN);		
		
	}
	
	/**
	 * @author Aravind Sankar
	 * Method to save the repeat invoice
	 */
	
	public static void clickSave(WebDriver driver)throws Exception{

		System.out.println("Saving...");		
		NewRepeatInvoicePage.btn_Save(driver).click();			
		
	}
	/**
	 * @author Aravind Sankar
	 *  Method returns the Item Description
	 */	
	public static String getDescription(WebDriver driver)throws Exception{

		String desc= NewRepeatInvoicePage.txt_Description(driver).getText();	
		System.out.println("Field  "+desc );
		
		return desc;
	
	}
	
	/**
	 * @author Aravind Sankar
	 *  Method returns the quantity
	 */
	public static String getQty(WebDriver driver)throws Exception{

		String qty= NewRepeatInvoicePage.txt_Qty(driver).getText();	
		System.out.println("Field  "+qty );
		
		return qty;
	
	}
	
	/**
	 * @author Aravind Sankar
	 *  Method returns the unit price
	 */
	public static String getUnitPrice(WebDriver driver)throws Exception{

		String unitPrice= NewRepeatInvoicePage.txt_UnitPrice(driver).getText();	
		System.out.println("Field  "+unitPrice );
		
		return unitPrice;		
		
	}
	
	/**
	 * @author Aravind Sankar
	 * Method returns the account detail
	 */
	public static String getAccount(WebDriver driver)throws Exception{

		String account= NewRepeatInvoicePage.txt_Account(driver).getText();	
		System.out.println("Field  "+account );		
		return account;		
		
	}
	/**
	 * @author Aravind Sankar
	 * returns errors generated
	 */
	public static String getErrors(WebDriver driver)throws Exception{

		String errors= NewRepeatInvoicePage.txt_Errors(driver).getText();	
		System.out.println("Field  "+errors );		
		return errors;		
		
	}

}
