package appModule;

import java.util.concurrent.TimeUnit;

import org.apache.log4j.xml.DOMConfigurator;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Reporter;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import utils.Constant;
import utils.ExcelUtils;
import utils.Log;
import appModule.HomeModule;
import appModule.LoginModule;
import appModule.NewRepeatInvoiceCreationModule;
import appModule.SalesModule;
import appModule.SearchRepeatModule;

public class VerifyErrorOnSaveWithoutItem {

	private static WebDriver driver = null;

	@BeforeMethod
	public void beforeMethod() throws Exception {
		DOMConfigurator.configure("log4j.xml");
		String projectPath= System.getProperty("user.dir");
		System.out.println(projectPath);
		String chromeDriverPath=projectPath+"\\chromedriver\\chromedriver.exe";
		System.setProperty("webdriver.chrome.driver",chromeDriverPath);
		Log.startTestCase("Check Login 1");
		ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_TestData,"Sheet1");
		driver= new ChromeDriver();
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		driver.get(Constant.URL);
	}
	@Test
	public void main() throws Exception {
		LoginModule.Execute(driver);
		driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);

		waitForLoad(driver,"dashboard");		
		HomeModule.Execute(driver);		

		waitForLoad(driver,"receivable");

		String ar=SalesModule.Execute(driver);

		//RepeatTransactions
		waitForLoad(driver,"searchrepeating");

		SearchRepeatModule.Execute(driver);

		waitForLoad(driver,"repeattransaction");

		NewRepeatInvoiceCreationModule.fillTransactionDetails(driver);

		NewRepeatInvoiceCreationModule.clickSave(driver);
		

		if(checkErrorMessages(driver,"Invoice to cannot be empty.","Invoice To")){
			Reporter.log("Verification passed");
		}
		if(checkErrorMessages(driver,"Description cannot be empty.","Description")){
			Reporter.log("Verification passed");
		}		
		System.out.println("done!!!  "+ar);

		//RepeatTransactions/Edit
	}
	public static void waitForLoad(WebDriver driver,String urlContains){

		(new WebDriverWait(driver, 10)).until(new ExpectedCondition<Boolean>() {
			public Boolean apply(WebDriver d) {
				return d.getCurrentUrl().toLowerCase().contains(urlContains);
			}
		});
	}
	public static Boolean checkErrorMessages(WebDriver driver,String compareText,String field)throws Exception{

		String errors= NewRepeatInvoiceCreationModule.getErrors(driver);

		if(errors.contains(compareText)){

			Reporter.log(" Error Verification Passed for "+ field);
			System.out.println("passed "+field);
			return true;

		}else{
			Reporter.log(" Error Verification Passed for "+ field);
			return false;
		}

	}
	@AfterMethod
	public void afterMethod() {
		//driver.quit();
	}

}
