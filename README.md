# README #

* Quick summary
Sample Test cases written in Selenium TestNG framework 
* V1.0

### How do I get set up? ###

* Summary of set up
It is a Eclipse EE project for selenium using TestNG framework.

* Configuration
##Download Eclipse EE at 
https://www.eclipse.org/downloads/packages/eclipse-ide-java-ee-developers/marsm2

##Steps to download TestNG framework in eclipse
http://testng.org/doc/download.html

##Steps to download chromewebdriver
https://code.google.com/p/selenium/wiki/ChromeDriver

download and check out the instructions here 

https://sites.google.com/a/chromium.org/chromedriver/downloads

or download directly here 

http://chromedriver.storage.googleapis.com/index.html?path=2.12/

place the 'chromedriver.exe' applicaiton file in the 'chromedriver' folder right under the project folder. 
E.g. if your project name is XERO, create a new folder under XERO, name it as 'chromedriver' 
and place the 'chromedriver.exe' downloaded in that folder.


* Dependencies

The jar files for the following are already added in the jar folder of the project
Selenium Driver
Apache Log4j
Apache POI

These files have to added in the build path under external libraries

* Contact
aravindtshankar@gmail.com