Steps to download chromewebdriver
https://code.google.com/p/selenium/wiki/ChromeDriver
download and check out the instructions here
https://sites.google.com/a/chromium.org/chromedriver/downloads
or download directly here
http://chromedriver.storage.googleapis.com/index.html?path=2.12/
place the 'chromedriver.exe' applicaiton file in the 'chromedriver' folder right under the project folder. E.g. if your project name is XERO, create a new folder under XERO, name it as 'chromedriver' and place the 'chromedriver.exe' downloaded in that folder.